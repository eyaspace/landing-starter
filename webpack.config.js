const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const BUILD_FOLDER = 'build';

module.exports = {
  entry: {
    index: path.resolve(__dirname, './src/index.js')
  },
  output: {
    path: path.resolve(__dirname, `${BUILD_FOLDER}/assets`),
    filename: '[name].js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader",
      query: {
        presets: ['latest']
      }
    }, {
      test: /fonts\/.+woff$/,
      loader: 'file-loader?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]'
    }, {
      test: /fonts\/.+woff2$/,
      loader: 'file-loader?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]'
    }, {
      test: /fonts\/.+[ot]tf$/,
      loader: 'file-loader?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]'
    }, {
      test: /fonts\/.+eot$/,
      loader: 'file-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]'
    }, {
      test: /fonts\/.+svg$/,
      loader: 'file-loader?limit=64990&mimetype=image/svg+xml&name=fonts/[name].[ext]'
    }, {
      test: /\.(png|jpg|svg)$/,
      loader: 'url-loader?limit=30000&name=[name].[ext]'
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: "css-loader"
      })
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: "css-loader!sass-loader"
      })
    }]
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    new CopyWebpackPlugin([{
      from: './src/images',
      to: './images'
    }]),
    new CopyWebpackPlugin([{
      from: './src/vendor',
      to: './vendor'
    }]),
    new CopyWebpackPlugin([{
      from: './public',
      to: '../'
    }])
    // new webpack.ProvidePlugin({
    //     jQuery: 'jquery',
    //     $: 'jquery',
    //     jquery: 'jquery'
    // })
  ]
};
