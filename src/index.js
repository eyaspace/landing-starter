// Uncomment if wanted

// import 'normalize.css';
// import 'flexboxgrid';

import './styles/index.scss';

import { detectMobile, detectMobileAndTablet } from './detect-mobile';
import { getMediaResources } from './get-media-resources';

// const elementAnimationEnd = (selector) => {
//   return new Promise((resolve) => {
//     jQuery(selector).on('animationend', () => resolve());
//   });
// };

const isMobile = detectMobile();
const isTablet = detectMobileAndTablet();

jQuery(onReady);

function onReady() {
  const body = jQuery('body');

  if (isMobile || jQuery(window).width() < 728) {
    body.removeClass('reveal loading')
        .addClass('mobile');
  } else if (isTablet) {
    body.removeClass('reveal loading')
        .addClass('tablet');
  } else {
      body.removeClass('loading').addClass('reveal');

    // Uncomment if skroller is required
    // initScrollr();
  }

  function initScrollr() {
    const baseH = 800;

    const s = skrollr.init({
      constants: {
        main: 0 * baseH,
        about: 0.3 * baseH,
        team: 2 * baseH,
        documents: 3 * baseH,
        hr: 4 * baseH,
        pricing: 5 * baseH
      }
    });

    skrollr.menu.init(s, {
      animate: true,
      easing: 'sqrt',
      scale: 50,
      duration: function (currentTop, targetTop) {
        return 1000;
      },
      updateUrl: false
    });
  }
}
