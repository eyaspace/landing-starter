
export const getMediaResources = () => {
  const resources = []
  jQuery('[src*=svg],[src*=png],[src*=jpg]').map((i, elm) => resources.push(jQuery(elm).attr('src')))

  return resources
};